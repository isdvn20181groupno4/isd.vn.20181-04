import java.util.Arrays;
import java.util.Scanner;

public class HelloWorld {
	private String name;
	private int day, month, year;
	
	public HelloWorld() {};	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public void sayWithPerson() {
		System.out.println("Hello    : " + getName());
		System.out.println("Your age : " + DateUtil.calculateAge(getYear()));
	}
	
	public void setBirthday() {
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Your name: ");
		this.name = reader.nextLine();

		System.out.print("Your birthday (ĐD/MM/YYYY): ");
		String date = reader.nextLine(); 
		while (!this.validBirthday(date)){
			System.out.print("Your birthday again (ĐD/MM/YYYY): ");
			date = reader.nextLine(); 
		}
	}
	
	public boolean validBirthday(String date) {
		String[] date_str = date.split("[\\/\\-+]");
		if (date_str.length != 3) return false;
		else {	
			try {
				this.day   = Integer.parseInt(date_str[0]);
				this.month = Integer.parseInt(date_str[1]);
				this.year  = Integer.parseInt(date_str[2]);
			} catch (NumberFormatException ex) {
				return false;
			}
			return DateUtil.checkDate(this.day, this.month, this.year) ? true : false; 
		}
	}
}
