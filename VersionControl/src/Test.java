
public class Test {

	public static void main(String[] args) {
		System.out.println("-----DateUtil test------");		
		System.out.println("checkDate(28, 2, 2000) : " + DateUtil.checkDate(28, 2, 2000));
		System.out.println("calculateAge(1997)     : " + DateUtil.calculateAge(1997));
		
		System.out.println("\n-----NumberUtil test------");		
		NumberUtil n = new NumberUtil();
		System.out.println("checkPrime(37)    : " + NumberUtil.checkPrime(37));
		System.out.println("perfectNumber(28) : " + NumberUtil.perfectNumber(28));
		System.out.println("squareNumber(16)  : " + NumberUtil.squareNumber(16));
		
		System.out.println("\n-----HelloWorld test------");
		HelloWorld person = new HelloWorld();
		person.setBirthday();
		person.sayWithPerson();
		
		System.out.println("\n-----FirstNumberApp test------");
		FirstNumberApp first = new FirstNumberApp();
		first.enterNumber();
		first.displayNumber();
		
		System.out.println("----- End -----");
	}
	
}
