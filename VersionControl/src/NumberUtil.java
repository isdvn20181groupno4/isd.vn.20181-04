
public class NumberUtil {
	public NumberUtil() {};
	
	public static boolean checkPrime (int number) {
		for (int i = 2; i < number; i++) {
			if (number%i == 0)
				return false;	// composite
		}
		return true;	// prime
	}
	
	public static boolean perfectNumber(int number) {
		int check = 0;
		
		for(int i=1; i<number ; i++) {
			if(number % i == 0) {
				check = check + i;
			}
		}
		
		if(check == number)
			return true;
		else
			return false;
	}
	
	public static boolean squareNumber (int number) {
		double squareNumber = Math.sqrt((double)number);
		int intVal = (int)squareNumber;
		
		if (intVal == squareNumber) {
			return true;
		}
		
		return false;
	}
}
