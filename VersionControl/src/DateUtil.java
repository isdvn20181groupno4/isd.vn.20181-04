import java.util.Calendar;

public class DateUtil {
	
	public static boolean checkDate(int day, int month, int year) {
		if(month < 0 || month > 12)
			return false;
		if(month == 2) {
	          if (year%100 == 0 && year%400 == 0) {
	                  if(day > 0 && day <= 28)
	                          return true;
	          } else if (year%4 == 0) {
	                  if(day > 0 && day <= 29 ) 
	                          return true;
	          } else {
	                  if(day > 0 && day <= 28)
	                          return true;
	          }
	    } else {
              if(day > 0 && day <= 28)
                      return true;
	    }
		
		return false;
	}

	public static int calculateAge(int year) {
		int current_year = Calendar.getInstance().get(Calendar.YEAR);
		return current_year - year;
	}
}
