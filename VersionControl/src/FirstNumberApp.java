import java.util.InputMismatchException;
import java.util.Scanner;

public class FirstNumberApp {
	private int number;
	
	public FirstNumberApp() {};
	
	public void displayNumber() {
		if (NumberUtil.checkPrime(this.number))
			System.out.println(this.number + " is a prime");
		else System.out.println(this.number + " is a composite");
		
		if(NumberUtil.perfectNumber(this.number))
			System.out.println(this.number + " is perfect number");
		if(NumberUtil.squareNumber(this.number))
			System.out.println(this.number + " is square number");
	}
	
	public void enterNumber() {
		Scanner reader = new Scanner(System.in);  
		System.out.print("Enter number : ");
		while(true) {
			try{
			    this.number = reader.nextInt();
			    return;
			}catch (InputMismatchException ex) {
				System.out.println("Enter number again : ");
				reader.next();
			    continue;
			}
		}
	}
}
