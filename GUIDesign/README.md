Phân công công việc tuần 6
#-----------------------------#
Duy  : 
	- Thiết kế luồng điều khiển màn hình
	- Design và đặc tả màn hình 
		+ Tạo lộ trình (Offer a ride : Enter pick up & drop-off + Passenger Contribution)
		+ Danh sách lộ trình đã tạo (Rides offered)
		+ Cập nhật thông tin cá nhân (Change profle)

#-----------------------------#
Kiên : 
	- Tạo các mockup 
	- Design và đặc tả màn hình : 
		+ Tìm kiếm lộ trình (Find a ride)
		+ Xem thông tin lộ trình + gửi yêu cầu đi chung (Ride detail + booking) 
		+ Đăng ký (Sign up)
		+ Đăng nhập (Login)

#-----------------------------#
Thái : 
	- Tạo các mockup
	- Design và đặc tả màn hình : 
		+ Quản lý thông tin + yêu cầu đi chung (Detail + list passenger)
		+ Xem danh sách lộ trình đã booking (Rides booked)
		+ Đổi mật khẩu (Change password)

