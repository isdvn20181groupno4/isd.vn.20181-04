package MVC.class.Validation;

import MVC.class.Datatype.Array;

public interface Validation {

	public abstract Array rule();

	public abstract Array message();

}
