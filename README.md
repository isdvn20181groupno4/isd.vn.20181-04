```
Giảng viên:
Nguyễn Thị Thu Trang

MSSV:
Hoàng Trung Kiên - 20152050
Vũ Công Duy - 20150636
Nguyễn Viết Thái - 20153356
```

## VersionControl

### I. Description of the homework/project

1. Description

- Homework 01. Version control with Git and Bitbucket

2. Preparing environment

- Install IDE e.g. Eclipse or Netbean
- Install Git (and/or Source Tree, eGit for Eclipse...)

3. Starting with Bitbucket

### II. Assignment of each member

1. Hoàng Trung Kiên

```
+ DateUtils class:
  - check date
+ NumberUtil class:
  - check a square number
+ HelloWorld class:
  - input birthday
+ FirstNumberApp class:
  - display prime or composite
```

2. Vũ Công Duy

```
  + Vẽ sơ đồ use case: 
	+ Vễ sơ đồ activity: map_pathway(ghép đôi), system_use 
	+ Đặc tả các UC : thiết lập lại mật khẩu, đăng yêu cầu chia sẻ xe, ghép đôi
```

3. Nguyễn Viết Thái

```
+ NumberUtil class:
  - check perfect number
+ HelloWorld class:
  - display age
+ FirstNumberApp class:
  - display square number and perfect number
```

### III. Reviewer for each assignment in your group
